﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class LaserController : MonoBehaviour
{

	private GameObject player;
	
	// Use this for initialization
	void Start () {
		player = GameObject.Find("rocket");
		
		//親子関係を設定
		transform.SetParent(player.transform, true);
	}
	
	// Update is called once per frame
	void Update ()
	{
		//レーザーのローカル座標(ロケットから見た座標系)を固定
		transform.localPosition = new Vector3(0,5,0);
		
		if (Input.GetKeyUp(KeyCode.Space) || CrossPlatformInputManager.GetButtonUp("Shot"))
		{
			Destroy(gameObject);
		}
	}
	
	// レーザーの衝突判定は今回はいらない
	/*void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.transform.tag == "Enemy")
		{
			Destroy(col.gameObject);
		}
	}*/
}
