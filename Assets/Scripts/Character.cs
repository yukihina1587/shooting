﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Rigidbody2Dコンポーネントを必須にする
[RequireComponent(typeof(Rigidbody2D))]

public class Character : MonoBehaviour
{

    // 機体の移動スピードを格納する変数
    public float speed = 5;

    // 機体の体力を示す変数
    private int HP = 0;

    // 弾を撃つ間隔を格納する変数
    public float shotDelay = 2.0f;

    // 弾のプレハブを格納する変数
    public GameObject bulletPrefab;
    
    // ミサイルのプレハブを格納する変数
    public GameObject missilePrefab;

    // レーザーのプレハブを格納する変数
    public GameObject laserPrefab;

    // Rigidbody2D コンポーネントを格納する変数
    private Rigidbody2D rb;

    // ゲーム起動時の処理
    void Awake()
    {
        // Rigidbody2D コンポーネントを取得して変数 rb に格納
        rb = GetComponent<Rigidbody2D>();
    }

    // 弾を作成する処理
    public void Shot(Transform origin)
    {
        // 弾を引数 origin と同じ位置/角度で作成
        Instantiate(bulletPrefab, origin.position, origin.rotation);
    }
    
    // ミサイルメソッドのオーバーロード
    // ミサイルを作成する処理
    public void Missile(Transform origin)
    {
        // ミサイルを引数 origin と同じ位置で作成
        Instantiate(missilePrefab, origin.position, origin.rotation);
    }
    
    // レーザーメソッドのオーバーロード
    // レーザーを作成する処理
    public void Laser(Transform origin)
    {
        Vector3 point = origin.position;    //レーザーの発射口の座標
        float start = 4.0f;
        
        point.y += start;
        
        // 弾を引数 origin と同じ位置/角度で作成
        Instantiate(laserPrefab, point, origin.rotation);
    }
    
    // 3way弾メソッドのオーバーロード
    // 3way弾を作成する処理
    public void Threeway(Transform origin)
    {
        Transform threewayshot = origin.transform;                
        threewayshot.Rotate(0.0f, 0.0f, -15.0f);
        this.Shot(threewayshot);
        threewayshot.Rotate(0.0f, 0.0f, 15.0f);
        this.Shot(threewayshot);
        threewayshot.Rotate(0.0f, 0.0f, 15.0f);
        this.Shot(threewayshot);
        threewayshot.Rotate(0.0f, 0.0f, -15.0f);
    }

    // 機体の移動処理
    public void Move(Vector2 direction)
    {
        // Rigidbody2D コンポーネントの velocity に方向と移動速度を掛けた値を渡す
        rb.velocity = direction * speed;
    }

    public void setHP(int h)
    {
        HP = h;
    }

    public int getHP()
    {
        return HP;
    }

    public void calcHP(int damage)
    {
        HP = HP + damage;
    }
}
