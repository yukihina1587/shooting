﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	/// スプライトをリソースから取得する.
	/// ※スプライトは「Resources/Sprites」以下に配置していなければなりません.
	/// ※fileNameに空文字（""）を指定するとシングルスプライトから取得します.
	public static Sprite GetSprite(string fileName, string spriteName) {
		if(spriteName == "") {
			// シングルスプライト
			return Resources.Load<Sprite>(fileName);
		}
		else {
			// マルチスプライト
			Sprite[] sprites = Resources.LoadAll<Sprite>(fileName);
			return System.Array.Find<Sprite>(sprites, (sprite) =>  sprite.name.Equals(spriteName));
		}
	}

	private static Rect _guiRect = new Rect();
	static Rect GetGUIRect() {
		return _guiRect;
	} 
	private static GUIStyle _guiStyle = null;
	static GUIStyle GetGUIStyle() {
		return _guiStyle ?? (_guiStyle = new GUIStyle());
	}
	/// フォントサイズを設定.
	public static void SetFontSize(int size) {
		GetGUIStyle().fontSize = size;
	}
	/// フォントカラーを設定.
	public static void SetFontColor(Color color) {
		GetGUIStyle().normal.textColor = color;
	}
	/// フォント位置設定
	public static void SetFontAlignment(TextAnchor align)
	{
		GetGUIStyle().alignment = align;
	}
	/// ラベルの描画.
	public static void GUILabel(float x, float y, float w, float h, string text) {
		Rect rect = GetGUIRect();
		rect.x = x;
		rect.y = y;
		rect.width = w;
		rect.height = h;

		GUI.Label(rect, text, GetGUIStyle());
	}
	/// ボタンの配置.
	public static bool GUIButton(float x, float y, float w, float h, string text) {
		Rect rect = GetGUIRect();
		rect.x = x;
		rect.y = y;
		rect.width = w;
		rect.height = h;

		return GUI.Button(rect, text, GetGUIStyle());
	}
}
