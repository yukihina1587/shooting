using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MissileController : MonoBehaviour
{
    // Use this for initialization

    [SerializeField,Header("ミサイル索敵範囲")]
    float MISSILE_SEARCH_LENGTH=0.5f;

    [SerializeField, Header("速度")]
    float speed = 1.0f;

    
    [HideInInspector]
    public GameObject player;

    void Start () {
        player = GameObject.Find("rocket");
    }

    GameObject lockon_target;

	// Update is called once per frame
	void Update ()
    {
        //ターゲットが未設定の場合
        if(!lockon_target)
        {
            //ミサイル前方(y+1)付近（範囲：MISSILE_SEARCH_LENGTH)の敵を全て取得(ただしロケット自身（あらかじめrocketのlayerを8,missileのlayerを9に設定）を除外するために~(1 << 8|1<<9)と記述)
            //また、敵の攻撃に反応してしまうためlayerを14に設定し、除外するため、1<<14を記述
            
            //CAUTION:
            //取得対象からミサイル自身とロケットを除外しないと自分自身を追尾することになるので気をつける
            //バグの原因になりかねるので自身のすべての弾、画面外の壁などのオブジェクトをlayermaskで除外する
            var near_enemis = Physics2D.CircleCastAll(transform.position+new Vector3(0,1,0), MISSILE_SEARCH_LENGTH, Vector2.zero, Mathf.Infinity, ~(1 << 8|1<<9|1<<10|1<<11|1<<12|1<<14|1<<15));
            
            //付近に敵がいた場合
            if(near_enemis.Length>0)
            {
                //一定範囲に複数の敵がいた場合を考慮し、ループで最も近い敵を選定する

                //一旦配列near_enemisの0番目をターゲットに設定
                lockon_target = near_enemis[0].collider.gameObject;
               
                foreach(var enemy in near_enemis)
                {
                    if (enemy.collider.gameObject.GetInstanceID() == lockon_target.GetInstanceID()) continue;

                    //ミサイルと現在のターゲットの距離・ミサイルとenemyの距離を比較(ただし2Dゲームなのでz座標を計算から外すためにVector2D.Length()を使用)
                    float missile_to_current_target = Vector2.Distance(transform.position, lockon_target.transform.position);
                    float missile_to_found_target = Vector2.Distance(transform.position, enemy.transform.position);

                    //最も近い敵をターゲットに設定
                    if(missile_to_found_target < missile_to_current_target)
                    {
                        lockon_target = enemy.collider.gameObject;
                       
                    }  
                }
            }

           
        }
        if (lockon_target)
        {
            //ベクトルの正規化
            Vector2 vec = (lockon_target.transform.position - transform.position).normalized;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.FromToRotation(Vector3.up, vec), 0.1f);
        }

        transform.Translate(transform.up*speed*Time.deltaTime);
        
        //画面外に出ると破壊
        if (transform.position.x > 6 || transform.position.x < -6 || transform.position.y > 6 || transform.position.y < -6){
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.transform.tag == "Enemy")
        {
            Destroy(this.gameObject);
        }
    }
}
