﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour {

    public GameObject EnemyPrefab;
    //EnemyController script;
    private int EnemyCount = 0;
    //音源
    public AudioClip EnemyGenSound;
    private AudioSource audioSource;
    private int MAX_Enemy = 0;


    // Use this for initialization
    void Start () {
        setMAX_Enemy(2);

        audioSource = gameObject.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
		//設定した時間（単位は秒）後にメソッドを呼び出し、repeatRate 秒ごとにリピートします
		InvokeRepeating("GenEnemy", 0, 1);
	    if (GameObject.Find("Canvas").GetComponent<UIController>().getgameScore() > 5000)
	    {
	        setMAX_Enemy(3);
	        if (GameObject.Find("Canvas").GetComponent<UIController>().getgameScore() > 10000)
	        {
	            setMAX_Enemy(4);
		        if (GameObject.Find("Canvas").GetComponent<UIController>().getgameScore() > 20000)
		        {
			        setMAX_Enemy(6);
		        }
	        }
	    }
	}

    void setMAX_Enemy(int e)
    {
        this.MAX_Enemy = e;
    }

    void GenEnemy()
    {
	    int e = this.getEnemyCount();
	    if (this.getEnemyCount() < MAX_Enemy)
        {
            //Instantiate関数（ゲームオブジェクト,敵を生成する位置,敵の角度）
            Instantiate(EnemyPrefab, new Vector3(-2.5f + 5 * Random.value, -0.5f + 3 * Random.value, 0), Quaternion.identity);
            audioSource.clip = EnemyGenSound;
            audioSource.Play();
	        e++;
	        this.setEnemyCount(e);
        }
    }

    public int getEnemyCount()
    {
        return EnemyCount;
    }

    public void setEnemyCount(int e)
    {
        EnemyCount = e;
    }
}
