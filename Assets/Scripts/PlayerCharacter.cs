﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using Debug = UnityEngine.Debug;

public class PlayerCharacter : Character {

    // 装備を切り替え格納する変数
    private int GameMode = 0;
    //音源
    public AudioClip BulletSound;
    public AudioClip MissileSound;
    public AudioClip LaserSound;
    public AudioClip SwitchSound;
    public AudioClip DamageSound;
    private AudioSource audioSource;

    // Use this for initialization
    void Start ()
    {
        this.setHP(100);
        audioSource = gameObject.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
    void FixedUpdate()
    {
        // 右・左のデジタル入力値を x に渡す
        float x = CrossPlatformInputManager.GetAxisRaw("Horizontal");
        // 上・下のデジタル入力値 y に渡す
        float y = CrossPlatformInputManager.GetAxisRaw("Vertical");
        // 移動する向きを求める
        // x と y の入力値を正規化して direction に渡す
        Vector2 direction = new Vector2(x, y).normalized;
        // Spaceship コンポーネントの Move() 処理を実行
        this.Move(direction);
        //回転固定
        this.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

        if (Input.GetKeyDown(KeyCode.E) || CrossPlatformInputManager.GetButtonDown("Switch"))
        {
            audioSource.clip = SwitchSound;
            audioSource.Play();
            switch (GameMode)
            {
                case 0:
                    GameMode = 1; //装備をミサイルに切り替え
                    //ミサイルへの切り替えの表示
                    GameObject.Find ("Canvas").GetComponent<UIController> ().SwitchMissile();
                    break;
                case 1:
                    GameMode = 2; //装備をレーザーに切り替え
                    //レーザーへの切り替えの表示
                    GameObject.Find ("Canvas").GetComponent<UIController> ().SwitchLaser();
                    break;
                case 2:
                    GameMode = 3; //装備を3way弾に切り替え
                    //3way弾への切り替えの表示
                    GameObject.Find ("Canvas").GetComponent<UIController> ().SwitchThreeway();
                    break;
                case 3:
                    GameMode = 0; //装備を通常弾に切り替え
                    //通常弾への切り替えの表示
                    GameObject.Find ("Canvas").GetComponent<UIController> ().SwitchBullet();
                    break;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) || CrossPlatformInputManager.GetButtonDown("Shot"))
        {
            switch (GameMode)
            {
                case 0:
                    StopCoroutine("Missile");
                    StopCoroutine("Laser");
                    StopCoroutine("Threeway");
                    //弾の発射処理（コルーチン Shot ）を実行
                    StartCoroutine("Shot");
                    audioSource.clip = BulletSound;
                    audioSource.Play();
                    break;
                case 1:
                    StopCoroutine("Shot");
                    StopCoroutine("Laser");
                    StopCoroutine("Threeway");
                    //ミサイルの発射処理（コルーチン Missile ）を実行
                    StartCoroutine("Missile");
                    audioSource.clip = MissileSound;
                    audioSource.Play();
                    break;
                case 2:
                    StopCoroutine("Shot");
                    StopCoroutine("Missile");
                    StopCoroutine("Threeway");
                    if (GameObject.Find("laserPrefab(Clone)") == null)
                    {
                        StartCoroutine("Laser");
                    }
                    audioSource.clip = LaserSound;
                    audioSource.Play();
                    break;
                case 3:
                    StopCoroutine("Shot");
                    StopCoroutine("Missile");
                    StopCoroutine("Laser");
                    //3way弾の発射処理（コルーチン Threeway ）を実行
                    StartCoroutine("Threeway");
                    audioSource.clip = BulletSound;
                    audioSource.Play();
                    audioSource.Play();
                    audioSource.Play();
                    break;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.transform.tag == "Enemy")
        {
            this.calcHP(-5);
            audioSource.clip = DamageSound;
            audioSource.Play();

            if (this.getHP() <= 0)
            {
                Destroy(this.gameObject);
                //ゲームオーバーフラグをtrueに
                GameObject.Find ("Canvas").GetComponent<UIController> ().setgameoverflag(true);
                //ゲームオーバーの表示
                GameObject.Find ("Canvas").GetComponent<UIController> ().GameOver ();
            }
        }
    }

    // 弾の発射処理（コルーチン）
    IEnumerator Shot(){
        // 弾をプレイヤーと同じ位置/角度で作成
        this.Shot(transform);
        // shotDelay 秒待つ
        yield return new WaitForSeconds(this.shotDelay);
    }

    // ミサイルの発射処理（コルーチン）
    IEnumerator Missile(){
        // 弾をプレイヤーと同じ位置/角度で作成
        this.Missile(transform);
        // shotDelay 秒待つ
        yield return new WaitForSeconds(this.shotDelay);
    }

    // レーザーの発射処理（コルーチン）
    IEnumerator Laser(){
        // 弾をプレイヤーと同じ位置/角度で作成
        this.Laser(transform);
        // shotDelay 秒待つ
        yield return new WaitForSeconds(this.shotDelay);
    }

    // 3way弾の発射処理（コルーチン）
    IEnumerator Threeway()
    {
        // 弾をプレイヤーと同じ位置/角度で作成
        this.Threeway(transform);
        
        // shotDelay 秒待つ
        yield return new WaitForSeconds(this.shotDelay);
    }
}
