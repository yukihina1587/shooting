﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCharacter : Character
{

    public GameObject explosionPrefab;
    private int EnemyID;
    private static int i = 0;
    public GameObject EnemyGen;
    
    // ゲームのスタート時の処理
    void Start()
    {
        // 敵のHPのセット
        this.setHP(50);
        i++;
        this.setID(i);
        // 弾の発射処理（コルーチン Shot ）を実行
        StartCoroutine("Shot");
        EnemyGen = GameObject.Find("EnemyGen");
    }

    private void Update()
    {
        for (int rad = 0; rad <= 4000; rad++)
        {
            float y = Mathf.Sin(rad)/120;
            /*if (transform.position.x >= -2.0f && transform.position.x <= 2.0f)
            {
                transform.Translate(-x, 0, 0);
            }
            else
            {*/
                transform.Translate(0, y, 0);
            //}
        }
    }

    void setID(int id)
    {
        EnemyID = id;
    }

    int getID()
    {
        return EnemyID;
    }
    
    // 衝突判定
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.transform.tag == "Bullet")
        {
            //エネミーのHPを更新
            this.calcHP(-5);
            
            if (this.getHP() <= 0)
            {
                Destroy(this.gameObject);
                
                //爆発エフェクトの生成
                Instantiate (explosionPrefab, transform.position, Quaternion.identity);
                
                //倒したときにスコアを更新する
                GameObject.Find ("Canvas").GetComponent<UIController> ().AddScore ();

                EnemyGenerator e = EnemyGen.GetComponent<EnemyGenerator>();

                int ec = e.getEnemyCount();
                ec = ec - 1;
                e.setEnemyCount(ec);
            }
        }
    }

    // 弾の発射処理（コルーチン）
    IEnumerator Shot()
    {
        while (true)
        {
            for(int rad = 0; rad < 360; rad += 15)
            {
                //自機の位置から射出する回転角を計算し格納
                Transform circleshot = this.transform;
                circleshot.Rotate(0.0f, 0.0f, rad);
                this.Shot(circleshot);
            }
            // shotDelay 秒待つ
            yield return new WaitForSeconds(this.shotDelay);
        }
    }
}
