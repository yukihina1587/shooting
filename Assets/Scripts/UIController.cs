﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	int score = 0;
	GameObject scoreText;
	GameObject gameOverText;
	private GameObject switchText;
	private GameObject finishscore;
	private bool gameoverflag = false;
	//ゲームオーバー
	public AudioClip GameOverSound;
	//ゲームプレイ中
	public AudioClip GamePlaySound;
	private AudioSource audioSource;
	//以下ハイスコア保存用
	public Text highScoreText; //ハイスコアを表示するText
    private int[] highScore = new int[5]; //ハイスコア用変数
    private string key; //ハイスコアの保存先キー


	public void AddScore(){
		this.score += 100;
	}

	public void AddBulletScore()
	{
		this.score += 10;
	}

	public void GameOver(){
		if (this.getgameoverflag() == true)
		{
			this.gameOverText.GetComponent<Text>().text = "GameOver";
			finishscore.GetComponent<Text>().text = "Score:" + this.getgameScore().ToString("D5");
			audioSource.clip = GameOverSound;
			audioSource.Play();
			gameoverflag = true;
			highScoreText.text = "RANK\n1st: " + highScore[0].ToString() + "\n2nd: " + highScore[1].ToString() + "\n3rd: " + highScore[2].ToString() + "\n4th: " + highScore[3].ToString() + "\n5th: " + highScore[4].ToString();
			//ハイスコアを表示
		}
	}

	public void setgameoverflag(bool flag)
	{
		this.gameoverflag = flag;
	}
	
	public bool getgameoverflag()
	{
		return gameoverflag;
	}

	public int getgameScore()
	{
		return this.score;
	}

	public void SwitchMissile()
	{
		this.switchText.GetComponent<Text>().text = "Missile";
		//this.sprite.GetComponent<Image>().sprite = Resources.Load<Sprite>("Assets/Resources/ミサイルアイコン.png");
	}
	
	public void SwitchLaser()
	{
		this.switchText.GetComponent<Text>().text = "Laser";
	}
	
	public void SwitchThreeway()
	{
		this.switchText.GetComponent<Text>().text = "Threeway";
	}
	
	public void SwitchBullet()
	{
		this.switchText.GetComponent<Text>().text = "Bullet";
	}
	
	void Start () {
		this.scoreText = GameObject.Find ("Score");
		this.gameOverText = GameObject.Find ("GameOver");
		this.switchText = GameObject.Find("SwitchText");
		this.switchText.GetComponent<Text>().text = "Bullet";
		this.finishscore = GameObject.Find("ScoreText");
		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.clip = GamePlaySound;
		audioSource.Play();
		
		//ハイスコア用
		for (int rank = 0; rank < 5; rank++)
		{
			highScore[rank] = PlayerPrefs.GetInt(rank.ToString(), 0);
		}
	}

	void Update () {
		scoreText.GetComponent<Text> ().text = "Score:" + this.getgameScore().ToString("D5");
		
		for (int rank = 4; rank > -1; rank--)
		{

			//ハイスコア最下位より現在のスコアが高い時
			if(highScore[rank] < this.score){  

				int tmp = highScore[rank];  

				highScore[rank] = this.score;

				if (rank < 4)
				{
					highScore[rank+1] = tmp;
				}
			}
		}

		//1~5位まで同じ得点にならず1位のみ該当得点になり、2~5位は0にする
		//2位が0なのに3~5位に得点が入ってしまう場合の対策も
		for (int rank = 3; rank > -1; rank--)
		{
			int rank_inverse = 3 - rank;
			if (highScore[rank] == highScore[rank + 1])
			{
				highScore[rank + 1] = 0;
			}

			if (highScore[rank_inverse] == 0)
			{
				highScore[rank_inverse + 1] = 0;
			}
		}

		for (int rank = 0; rank < 5; rank++)
		{
			PlayerPrefs.SetInt(rank.ToString(), highScore[rank]);
			//ハイスコアを保存
		}
	}
}
