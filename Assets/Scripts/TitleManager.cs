﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class TitleManager : MonoBehaviour
{

    //音源
    public AudioClip GamePlaySound;
    private AudioSource audioSource;
    
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        audioSource.clip = GamePlaySound;
        audioSource.Play();
    }
    void Update() {
        // タッチされているかチェック
        if (Input.touchCount > 0) {
            // タッチ情報の取得
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began) {
                //Debug.Log("押した瞬間");
                SceneManager.LoadScene("shooting_game");
            }

            /*if (touch.phase == TouchPhase.Ended) {
                Debug.Log("離した瞬間");
            }

            if (touch.phase == TouchPhase.Moved) {
                Debug.Log("押しっぱなし");
            }*/
        }

        if (CrossPlatformInputManager.GetButtonDown("Start"))
        {
            SceneManager.LoadScene("shooting_game");
        }
    }
}
