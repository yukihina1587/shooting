﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ReturnToTitle : MonoBehaviour {

	void Start () {
		
	}

	void Update()
	{
		if (GameObject.Find("Canvas").GetComponent<UIController>().getgameoverflag() == true)
		{
			// タッチされているかチェック
			if (Input.touchCount > 0)
			{
				// タッチ情報の取得
				Touch touch = Input.GetTouch(0);

				if (touch.phase == TouchPhase.Began)
				{
					SceneManager.LoadScene("Title");
				}
			}
		}
	}
}
