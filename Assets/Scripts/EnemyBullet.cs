﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, 0.03f, 0);

        if (transform.position.x > 6 || transform.position.x < -6 || transform.position.y > 6 || transform.position.y < -6)
        {
            Destroy(gameObject);
        }
    }
    
    // 衝突判定
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.transform.tag == "Bullet" || col.gameObject.transform.tag == "Player")
        {
            Destroy(this.gameObject);
            if (col.gameObject.transform.tag == "Bullet")
            {
                //倒したときにスコアを更新する
                GameObject.Find ("Canvas").GetComponent<UIController> ().AddBulletScore ();
            }
        }
    }
}
