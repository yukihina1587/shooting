﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleRocketMoving : Character
{
	
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		/*for (int rad = 0; rad <= 4000; rad++)
		{
			float x = (Mathf.Cos(rad)/20)*0.8f;
			
			transform.Translate(x, 0, 0);
		}*/
	}

	void First()
	{
		this.Move(new Vector2(-1, 0).normalized);
	}

	void Second()
	{
		
	}
	
	// 弾の発射処理（コルーチン）
	IEnumerator Shot(){
		// 弾をプレイヤーと同じ位置/角度で作成
		this.Shot(transform);
		// shotDelay 秒待つ
		yield return new WaitForSeconds(this.shotDelay);
	}

	// ミサイルの発射処理（コルーチン）
	IEnumerator Missile(){
		// 弾をプレイヤーと同じ位置/角度で作成
		this.Missile(transform);
		// shotDelay 秒待つ
		yield return new WaitForSeconds(this.shotDelay);
	}

	// レーザーの発射処理（コルーチン）
	IEnumerator Laser(){
		// 弾をプレイヤーと同じ位置/角度で作成
		this.Laser(transform);
		// shotDelay 秒待つ
		yield return new WaitForSeconds(this.shotDelay);
	}

	// 3way弾の発射処理（コルーチン）
	IEnumerator Threeway()
	{
		// 弾をプレイヤーと同じ位置/角度で作成
		this.Threeway(transform);
        
		// shotDelay 秒待つ
		yield return new WaitForSeconds(this.shotDelay);
	}
}
